//import axios from 'axios';
import axiosInstance from './axiosInstance';

const GetEmployees = () => {
  return axiosInstance.get('http://dummy.restapiexample.com/api/v1/employees')
    .then((response) => {
      console.log('ResponseData: ' + JSON.stringify(response.data.data));
      return response.data.data;
    }).catch((error) => {
      console.log(error);
    });
};
export default {
  GetEmployees,
};
