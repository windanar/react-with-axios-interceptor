import React, { Component } from 'react';
import './App.css';
import Service from './service';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      name: 'No Name',
    };
  }

  componentDidMount() {
    console.log('CP1 - DidMount');
    this.onLoad();
  }

  onLoad = () => {
    //Service.GetEmployees(); // NoNeed
    // Service.putTest(); // NoNeed
    Service.GetEmployees().then((data) => {
      console.log(data[0].employee_name);
      this.setState({ name: data[0].employee_name });
    });
  }


  render() {
    return (
      <div className="App">
        Hi, ooo
        <div>
          {this.state.name}
        </div>
    </div>
    );
  }
}

export default App;
